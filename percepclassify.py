#!usr/bin/env python3

import os
import sys
import operator
import json



globalDict = {}

def classify(modelFileArg):
	print("xyz")
	modelFile = open(modelFileArg,"r")
	
	global globalDict
	classList = []
	
	modelData = json.loads(modelFile.read())
	modelFile.close()
	
	classList = modelData[0]["classList"]
	globalDict = modelData[1]["globalDict"]
	no_of_docs =0
	no_of_right_docs =0
	sumDict ={}
	sys.stdin = codecs.getreader('utf-8')(sys.stdin.detach(),errors="ignore")
	testdata = sys.stdin.readlines()
	for dev_list in testdata:
		dev_list = dev_list.rstrip("\n")
		no_of_docs +=1
		lineSplit = dev_list.split()
		globalDict.update(addWord(lineSplit[:]))
		
		for className in classList:	
			summation = 0
			for word in globalDict["featureDict"]:
				key = className+"AvgDict"
				if word in globalDict[key]:
					summation += (globalDict[key][word])
			sumDict[className] = summation
		if max(sumDict.items(), key=operator.itemgetter(1))[1] == 0:
			predictedLabel = classList[0]
		else:
			predictedLabel = max(sumDict.items(), key=operator.itemgetter(1))[0]
		sys.stdout.write(predictedLabel+"\n")


def addWord(dataList):
	
	global globalDict
	featureDict = {}
	for word in dataList:
		if word not in featureDict:
			featureDict[word] = 1
	globalDict["featureDict"] = featureDict
	return globalDict

def main():
	classify(sys.argv[1])
	
	
main()
