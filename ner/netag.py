#!usr/bin/env python3

import os
import sys
import json
import operator
import codecs


def classify(modelFileArg):
	modelFile = open(modelFileArg,"r")
	
	global globalDict
	classList = []
	
	modelData = json.loads(modelFile.read())
	modelFile.close()
	
	classList = modelData[0]["classList"]
	globalDict = modelData[1]["globalDict"]
	sys.stdin = codecs.getreader('utf-8')(sys.stdin.detach(),errors="ignore")
	testdata = sys.stdin.readlines()
	for dev_list in testdata:
		i =0
		dev_list = dev_list.rstrip("\n")
		dev_list = dev_list.rstrip(" ")
		featureVectorList = []
		predictedLabelList = []
		lineSplit = dev_list.split(" ")
		listl = []
		while i<len(lineSplit):
			curr = lineSplit[i].split("/")
			if len(curr) > 2:
				cword = '/'.join(curr[0:-1])
			else:
				cword = curr[0]
			
			if len(lineSplit) == 1:
				featureVectorList=["cword:"+cword,"ctag:"+curr[-1],"pword:START","ptag:START","pNER:START","nword:END","ntag:END","shape:"+shape(cword)]
			else:
				if i == 0:
					if len(curr) > 2:
						cword = '/'.join(curr[0:-1])
					else:
						cword = curr[0]
					next = lineSplit[i+1].split("/")
					if len(next) > 2:
						nword = '/'.join(next[0:-1])
					else:
						nword = next[0]
					
					featureVectorList=["cword:"+cword,"ctag:"+curr[-1],"pword:START","ptag:START","pNER:START","nword:"+nword,"ntag:"+next[-1],"shape:"+shape(cword)]

				elif i == len(lineSplit)-1:
					prev = lineSplit[i-1].split("/")
					if len(prev) > 2:
						pword = '/'.join(prev[0:-1])
					else:
						pword = prev[0]
					featureVectorList=["cword:"+cword,"ctag:"+curr[-1],"pword:"+pword,"ptag:"+prev[-1],"pNER:"+predictedLabelList[i-1],"nword:END","ntag:END","shape:"+shape(cword)]
				
				elif i == 1:
					prev = lineSplit[i-1].split("/")
					if len(prev) > 2:
						pword = '/'.join(prev[0:-1])
					else:
						pword = prev[0]

					next = lineSplit[i+1].split("/")
					if len(next) > 2:
						nword = '/'.join(next[0:-1])
					else:
						nword = next[0]

					featureVectorList=["cword:"+cword,"ctag:"+curr[-1],"pword:"+pword,"ptag:"+prev[-1],"pNER:"+predictedLabelList[i-1],"nword:"+nword,"ntag:"+next[-1],"shape:"+shape(cword)]
				
				else:
					prev = lineSplit[i-1].split("/")
					if len(prev) > 2:
						pword = '/'.join(prev[0:-1])
					else:
						pword = prev[0]

					next = lineSplit[i+1].split("/")
					if len(next) > 2:
						nword = '/'.join(next[0:-1])
					else:
						nword = next[0]

					featureVectorList=["cword:"+cword,"ctag:"+curr[-1],"pword:"+pword,"ptag:"+prev[-1],"pNER:"+predictedLabelList[i-1],"nword:"+nword,"ntag:"+next[-1],"shape:"+shape(cword)]
				

			sumDict ={}
			for className in classList:	
				summation = 0
				for word in featureVectorList:
					key = className+"AvgDict"
					if word in globalDict[key]:
						summation += (globalDict[key][word])
				sumDict[className] = summation
			if max(sumDict.items(), key=operator.itemgetter(1))[1] == 0:
				predictedLabel = classList[0]
			else:
				predictedLabel = max(sumDict.items(), key=operator.itemgetter(1))[0]
			
			predictedLabelList.append(predictedLabel)
			
			i = i+1
		j=0
		for word in lineSplit:
			sys.stdout.write(word+"/"+predictedLabelList[j]+" ")
			j=j+1
		sys.stdout.write("\n")
def shape(word):
	leng = len(word)
	s= ""
	for c in word:
		if c.isdigit():
			s += "9"
		elif c.islower():
			s += "a"
		elif c.isupper():
			s += "A"
		else:
			s+= "-"
	t =""				
	j=0
	i=0
	while i < leng:
		if s[i]=="a":
			t += "a"
			for j in range(i+1,leng):
				if s[j] != "a":
					break
				else:
					i = i+1
		
		if s[i]=="A":
			t += "A"
			for j in range(i+1,leng):
				if s[j] != "A":
					break
				else:
					i = i+1
		if s[i]=="9":
			t += "9"
			for j in range(i+1,leng):
				if s[j] != "9":
					break
				else:
					i = i+1
		if s[i]=="-":
			t += "-"
			for j in range(i+1,leng):
				if s[j] != "-":
					break
				else:
					i = i+1
		i = i+1
	return t

	
def main():
	classify(sys.argv[1])
	
main()
