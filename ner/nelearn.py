#!usr/bin/env python3

import sys
sys.path.insert(0,'..')
import perceplearn
import argparse


parser = argparse.ArgumentParser(add_help=False)
parser.add_argument('-h',dest="devfile",action='store')
parser.add_argument('trainfile')
parser.add_argument('modelfile')
result = parser.parse_args()

print(str(result.devfile)+result.trainfile+result.modelfile)

def form_input():
	
	#format training data
	ifile = open(result.trainfile,"rt",errors="ignore",encoding="utf-8")
	ofile = open("ner_training.txt","wt")
	pos_lines=ifile.readlines()
	format_data(pos_lines,ofile)
	ifile.close()

	#format dev data
	if result.devfile != None:
		ifile = open(result.devfile,"rt",errors="ignore")
		ofile = open("ner_dev.txt","wt")
		pos_lines=  ifile.readlines()
		ifile.close()
		format_data(pos_lines,ofile)
		perceplearn.learn("ner_training.txt",result.modelfile,"ner_dev.txt")
	else:
		perceplearn.learn("ner_training.txt",result.modelfile,None)
		

def format_data(pos_lines,ofile):
	for line in pos_lines:
		i=0
		lineSplit = line.split()
		leng = len(lineSplit)
		while i<leng:
			curr = lineSplit[i].split("/")
			if len(curr) > 3:
				cword = '/'.join(curr[0:-2])
			else:
				cword = curr[0]
			if leng == 1:
				ofile.write(curr[-1]+" ")
				ofile.write("cword:"+cword+" ")
				ofile.write("ctag:"+curr[-2]+" ")
				ofile.write("pword:"+"START"+" ")
				ofile.write("ptag:"+"START"+" ")
				ofile.write("pNER:"+"START"+" ")				
				ofile.write("nword:END"+" ")
				ofile.write("ntag:"+"END"+" ")
				ofile.write("shape:"+shape(cword))
				

			elif i==0:
				next = lineSplit[i+1].split("/")
				if len(next) > 3:
					nword = '/'.join(next[0:-2])
				else:
					nword = next[0]
				
				ofile.write(curr[-1]+" ")
				ofile.write("cword:"+cword+" ")
				ofile.write("ctag:"+curr[-2]+" ")
				ofile.write("pword:"+"START"+" ")
				ofile.write("ptag:"+"START"+" ")
				ofile.write("pNER:"+"START"+" ")				
				ofile.write("nword:"+nword+" ")				
				ofile.write("ntag:"+next[-2]+" ")
				ofile.write("shape:"+shape(cword))
							
			elif (i == len(lineSplit)-1):
				prev = lineSplit[i-1].split("/")
				if len(prev) > 3:
					pword = '/'.join(prev[0:-2])
				else:
					pword = prev[0]
				ofile.write(curr[-1]+" ")
				ofile.write("cword:"+cword+" ")
				ofile.write("ctag:"+curr[-2]+" ")
				ofile.write("pword:"+pword+" ")
				ofile.write("ptag:"+prev[-2]+" ")
				ofile.write("pNER:"+prev[-1]+" ")				
				ofile.write("nword:"+"END"+" ")				
				ofile.write("ntag:"+"END")
				ofile.write("shape:"+shape(cword))
				
			elif i==1:
				prev = lineSplit[i-1].split("/")
				if len(prev) > 3:
					pword = '/'.join(prev[0:-2])
				else:
					pword = prev[0]
				
				next = lineSplit[i+1].split("/")
				if len(next) > 3:
					nword = '/'.join(next[0:-2])
				else:
					nword = next[0]
				ofile.write(curr[-1]+" ")
				ofile.write("cword:"+cword+" ")
				ofile.write("ctag:"+curr[-2]+" ")
				ofile.write("pword:"+pword+" ")
				ofile.write("ptag:"+prev[-2]+" ")
				ofile.write("pNER:"+prev[-1]+" ")				
				ofile.write("nword:"+nword+" ")				
				ofile.write("ntag:"+next[-2]+" ")
				ofile.write("shape:"+shape(cword))
				
			else:
				
				
				prev = lineSplit[i-1].split("/")
				if len(prev) > 3:
					pword = '/'.join(prev[0:-2])
				else:
					pword = prev[0]
				
				next = lineSplit[i+1].split("/")
				if len(next) > 3:
					nword = '/'.join(next[0:-2])
				else:
					nword = next[0]
				ofile.write(curr[-1]+" ")
				ofile.write("cword:"+cword+" ")
				ofile.write("ctag:"+curr[-2]+" ")
				ofile.write("pword:"+pword+" ")
				ofile.write("ptag:"+prev[-2]+" ")
				ofile.write("pNER:"+prev[-1]+" ")				
				ofile.write("nword:"+nword+" ")				
				ofile.write("ntag:"+next[-2]+" ")
				ofile.write("shape:"+shape(cword))
				
			ofile.write("\n")
			i=i+1		
	
	ofile.close()
	print("frmatting done")

def shape(word):
	leng = len(word)
	s= ""
	for c in word:
		if c.isdigit():
			s += "9"
		elif c.islower():
			s += "a"
		elif c.isupper():
			s += "A"
		else:
			s+= "-"
	t =""				
	j=0
	i=0
	while i < leng:
		if s[i]=="a":
			t += "a"
			for j in range(i+1,leng):
				if s[j] != "a":
					break
				else:
					i = i+1
		
		if s[i]=="A":
			t += "A"
			for j in range(i+1,leng):
				if s[j] != "A":
					break
				else:
					i = i+1
		if s[i]=="9":
			t += "9"
			for j in range(i+1,leng):
				if s[j] != "9":
					break
				else:
					i = i+1
		if s[i]=="-":
			t += "-"
			for j in range(i+1,leng):
				if s[j] != "-":
					break
				else:
					i = i+1
		i = i+1
	return t

	
def main():
	form_input()

if __name__ == '__main__':
	main()
