#!usr/bin/env python3


import sys
sys.path.insert(0,"..")
import perceplearn
import argparse


parser = argparse.ArgumentParser(add_help=False)
parser.add_argument('-h',dest="devfile",action='store')
parser.add_argument('trainfile')
parser.add_argument('modelfile')
result = parser.parse_args()

print(str(result.devfile)+result.trainfile+result.modelfile)

def form_input():
	
	#format training data
	ifile = open(result.trainfile,"rt",errors="ignore",encoding="utf-8")
	ofile = open("pos_training.txt","wt")
	pos_lines=ifile.readlines()
	format_data(pos_lines,ofile)
	ifile.close()

	#format dev data
	if result.devfile != None:
		ifile = open(result.devfile,"rt",errors="ignore",encoding="utf-8")
		ofile = open("pos_dev.txt","wt")
		pos_lines=  ifile.readlines()
		ifile.close()
		format_data(pos_lines,ofile)
		perceplearn.learn("pos_training.txt",result.modelfile,"pos_dev.txt")
	else:
		perceplearn.learn("pos_training.txt",result.modelfile,None)
		

def format_data(pos_lines,ofile):
	for line in pos_lines:
		i=0
		lineSplit = line.split()
		leng = len(lineSplit)
		while i<leng:
			if leng == 1:
				curr = lineSplit[i].split("/")
				ofile.write(curr[1]+" ")
				cword = curr[0]
				ofile.write("cword:"+cword+" ")
				ofile.write("pptag:"+"START"+" ")
				ofile.write("ptag:"+"START"+" ")
				ofile.write("pword:"+"START"+" ")
				ofile.write("nword:END"+" ")
				pre2 = (cword[:2] or cword[:1])
				suf1 = (cword[-1:])
				suf2 = (cword[-2:] or cword[-1:])
				suf3 = (cword[-3:] or cword[-2:] or cword[-1:])
				ofile.write("pre2:"+pre2+" ")
				ofile.write("suf1:"+suf1+" ")
				ofile.write("suf2:"+suf2+" ")
				ofile.write("suf3:"+suf3+" ")
				ofile.write("shape:"+shape(cword))

			elif i==0:
				curr = lineSplit[i].split("/")
				next = lineSplit[i+1].split("/")
				cword = curr[0]
				ofile.write(curr[1]+" ")
				ofile.write("cword:"+cword+" ")
				ofile.write("pptag:"+"START"+" ")
				ofile.write("ptag:"+"START"+" ")
				ofile.write("pword:"+"START"+" ")
				ofile.write("nword:"+next[0]+" ")
				pre2 = (cword[:2] or cword[:1])
				suf1 = (cword[-1:])
				suf2 = (cword[-2:] or cword[-1:])
				suf3 = (cword[-3:] or cword[-2:] or cword[-1:])
				ofile.write("pre2:"+pre2+" ")
				ofile.write("suf1:"+suf1+" ")
				ofile.write("suf2:"+suf2+" ")
				ofile.write("suf3:"+suf3+" ")
				ofile.write("shape:"+shape(cword))
			
			elif (i == len(lineSplit)-1):
				curr = lineSplit[i].split("/")
				pprev = lineSplit[i-2].split("/")
				prev = lineSplit[i-1].split("/")
				ofile.write(curr[1]+" ")
				cword = curr[0] 
				ofile.write("cword:"+cword+" ")
				ofile.write("pptag:"+pprev[1]+" ")
				ofile.write("ptag:"+prev[1]+" ")
				ofile.write("pword:"+prev[0]+" ")
				ofile.write("nword:"+"END"+" ")
				pre2 = (cword[:2] or cword[:1])
				suf1 = (cword[-1:])
				suf2 = (cword[-2:] or cword[-1:])
				suf3 = (cword[-3:] or cword[-2:] or cword[-1:])
				ofile.write("pre2:"+pre2+" ")
				ofile.write("suf1:"+suf1+" ")
				ofile.write("suf2:"+suf2+" ")
				ofile.write("suf3:"+suf3+" ")
				ofile.write("shape:"+shape(cword))

			elif i==1:
				curr = lineSplit[i].split("/")
				cword = curr[0] 
				prev = lineSplit[i-1].split("/")
				next = lineSplit[i+1].split("/")
				ofile.write(curr[1]+" ")
				ofile.write("cword:"+cword+" ")
				ofile.write("pptag:START ")
				ofile.write("ptag:"+prev[1]+" ")
				ofile.write("pword:"+prev[0]+" ")
				ofile.write("nword:"+next[0]+" ")
				pre2 = (cword[:2] or cword[:1])
				suf1 = (cword[-1:])
				suf2 = (cword[-2:] or cword[-1:])
				suf3 = (cword[-3:] or cword[-2:] or cword[-1:])
				ofile.write("pre2:"+pre2+" ")
				ofile.write("suf1:"+suf1+" ")
				ofile.write("suf2:"+suf2+" ")
				ofile.write("suf3:"+suf3+" ")
				ofile.write("shape:"+shape(cword))

			else:
				
				curr = lineSplit[i].split("/")
				pprev = lineSplit[i-2].split("/")
				prev = lineSplit[i-1].split("/")
				next = lineSplit[i+1].split("/")
				ofile.write(curr[1]+" ")
				cword = curr[0] 
				ofile.write("cword:"+cword+" ")
				ofile.write("pptag:"+pprev[1]+" ")
				ofile.write("ptag:"+prev[1]+" ")
				ofile.write("pword:"+prev[0]+" ")
				ofile.write("nword:"+next[0]+" ")
				pre2 = (cword[:2] or cword[:1])
				suf1 = (cword[-1:])
				suf2 = (cword[-2:] or cword[-1:])
				suf3 = (cword[-3:] or cword[-2:] or cword[-1:])
				ofile.write("pre2:"+pre2+" ")
				ofile.write("suf1:"+suf1+" ")
				ofile.write("suf2:"+suf2+" ")
				ofile.write("suf3:"+suf3+" ")
				ofile.write("shape:"+shape(cword))

			ofile.write("\n")
			i=i+1		
	
	ofile.close()
	print("frmatting done")
	
def shape(word):
	leng = len(word)
	s= ""
	for c in word:
		if c.isdigit():
			s += "9"
		elif c.islower():
			s += "a"
		elif c.isupper():
			s += "A"
		else:
			s+= "-"
	t =""				
	j=0
	i=0
	while i < leng:
		if s[i]=="a":
			t += "a"
			for j in range(i+1,leng):
				if s[j] != "a":
					break
				else:
					i = i+1
		
		if s[i]=="A":
			t += "A"
			for j in range(i+1,leng):
				if s[j] != "A":
					break
				else:
					i = i+1
		if s[i]=="9":
			t += "9"
			for j in range(i+1,leng):
				if s[j] != "9":
					break
				else:
					i = i+1
		if s[i]=="-":
			t += "-"
			for j in range(i+1,leng):
				if s[j] != "-":
					break
				else:
					i = i+1
		i = i+1
	return t

	
def main():
	form_input()

if __name__ == '__main__':
	main()
