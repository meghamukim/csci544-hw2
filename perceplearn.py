#!usr/bin/env python3

import sys
import operator
import random
import json
import argparse


parser = argparse.ArgumentParser(add_help=False)
parser.add_argument('-h',dest="devfile",action='store')
parser.add_argument('trainfile')
parser.add_argument('modelfile')
result = parser.parse_args()

globalDict = {}

def learn(fileopen,modelFile,devfile):
	ifile = open(fileopen,"rt",errors="ignore",encoding="utf-8")
		
	global globalDict
	classList = []
	no_of_docs = 0
	no_of_right_docs = 0
	tr_list = ifile.readlines()
	docList = random.sample(tr_list,len(tr_list))
	training_set = len(docList)
	#to read all class names and form weight vectors for each class
	for line in docList:
		lineSplit = line.split()
		className = lineSplit[0]
		if className not in classList:
			classList.append(className)
			key = className+"WeightDict"
			globalDict[key] = {}
			key = className+"AvgDict"
			globalDict[key] = {}

	i=0
	N=15
	count=0
	param = training_set*N
	writeDict = {}
	while i<N:

		error = 0
		for line in docList:
			count+=1
			lineSplit = line.split()
			classCategory = lineSplit[0]
			globalDict["featureDict"]={}
			featureDict = {}
			
			for word in lineSplit[1:]:
				if word not in featureDict:
					featureDict[word] = 1
			globalDict["featureDict"] = featureDict
			
			sumDict = {}
			for className in classList:	
				summation = 0
				for word in globalDict["featureDict"]:
					key = className+"WeightDict"
					if word not in globalDict[key]:
						globalDict[key][word] = 0
					summation += (globalDict[key][word])
					
					key = className+"AvgDict"
					if word not in globalDict[key]:
						globalDict[key][word] = 0
				sumDict[className] = summation
			if max(sumDict.items(), key=operator.itemgetter(1))[1] == 0:
				predictedLabel = classList[0]
			else:
				predictedLabel = max(sumDict.items(), key=operator.itemgetter(1))[0]
						
			if predictedLabel != classCategory:
				error +=1
				for word in globalDict["featureDict"]:
					key = classCategory+"WeightDict"
					globalDict[key][word] += 1
					key = classCategory+"AvgDict"
					globalDict[key][word] += (globalDict["featureDict"][word] * (param-count)) 
					key = predictedLabel+"WeightDict"
					globalDict[key][word] -= 1
					key = predictedLabel+"AvgDict"
					globalDict[key][word] -= (globalDict["featureDict"][word] *(param-count))
		if devfile!= None:
			sumDict ={}
			devf = open(devfile,"rt",errors="ignore",encoding="utf-8")
			dev_list = devf.readlines()
			no_of_docs = 0
			no_of_right_docs = 0
			for doc in dev_list:
				no_of_docs +=1
				
				lineSplit = doc.split()
				actual = lineSplit[0]
				featureDict = {}
				globalDict["featureDict"] = {}
				for word in lineSplit[1:]:
					if word not in featureDict:
						featureDict[word] = 1
						globalDict["featureDict"] = featureDict
				
				for className in classList:	
					summation = 0
					for word in globalDict["featureDict"]:
						key = className+"AvgDict"
						if word in globalDict[key]:
							summation += (globalDict[key][word])
					sumDict[className] = summation
				if max(sumDict.items(), key=operator.itemgetter(1))[1] == 0:
					predictedLabel = classList[0]
				else:
					predictedLabel = max(sumDict.items(), key=operator.itemgetter(1))[0]
				
				if predictedLabel == actual:
					no_of_right_docs +=1
			accuracy = float(no_of_right_docs/no_of_docs)
			
			print(accuracy)
			print(i)
					
			if i==0:
				maxaccu = accuracy
			if accuracy >= maxaccu:
				writeDict = globalDict
				maxaccu = accuracy
		else:
			
			print(error)
			print(i)
			if i==0:
				maxaccu = error
			if error <= maxaccu:
				writeDict = globalDict
				maxaccu = error
			
		i = i+1
		
	ifile.close()
	ofile = open(modelFile,"wt")
	data = [{"classList":classList},{"globalDict": writeDict}]
	json.dump(data,ofile)
	ofile.close()	

	
def main():
	learn(result.trainfile,result.modelfile,result.devfile)

if __name__ == '__main__':
	main()
