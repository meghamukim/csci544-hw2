Part I:

Execution command 
1. python3 perceplearn.py TRAININGFILE MODELFILE -h DEVFILE
2. python3 percepclassify.py MODELFILE < INPUTFILE > OUTPUTFILE

Part II:

Execution command 
1. python3 postrain.py TRAININGFILE MODELFILE -h DEVFILE
2. python3 postag.py MODELFILE < INPUTFILE > OUTPUTFILE

Part III:

Execution command 
1. python3 nelearn.py TRAININGFILE MODELFILE -h DEVFILE
2. python3 netag.py MODELFILE < INPUTFILE > OUTPUTFILE

Part IV:

a) Accuracy of Part-of-Speech tagging(POS Tagging) : 96.46%

b) B-LOC entity:
	Precision: 0.64
	Recall:	0.77
	F-Score: 0.70

   B-ORG entity:
	Precision: 0.81
	Recall:	0.73
	F-Score: 0.77

   B-PER entity:
	Precision: 0.84
	Recall:	0.81
	F-Score: 0.82

   B-MISC entity:
	Precision: 0.60
	Recall:	0.51
	F-Score: 0.55

Overall F-Score for NER: 0.71

c) Performance metrics for Naive Baye's Classifier on Part-of-Speech Tagging:

	Accuracy of Part-of-Speech Tagging (POS Tagging): 93.46%

Naive Bayes Classifier is faster than the Average Multiclass Perceptron but the accuracy is poor comparatively when we use the Naive Bayes Classifier. This is because the NB Classifier does not take into account the context of the text. It follows the 'Independence Assumption' which leads to ignoring the surrounding context. It calculates the probability of individual word according to its occurrence in the particular document irrespective of its context. A word might have a different context in different documents. For example: The dog was hit & The dog show was a hit. When NB Classifier classifies the word 'hit' it takes into account the number of occurrence in both the documents irrespective of how is the word used in each document. Whereas in Average Multi Class Perceptron the context is taken into consideration using the Forward/Backward classification to classify each word.